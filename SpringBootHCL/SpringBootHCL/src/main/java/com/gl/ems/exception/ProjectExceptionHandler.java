package com.gl.ems.exception;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.gl.ems.beans.ResponseMessage;



@RestControllerAdvice
public class ProjectExceptionHandler {
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<ResponseMessage> handleException(HttpServletRequest request, Exception ex){
		ResponseMessage rm = new ResponseMessage();
		rm.setMessage(ex.getMessage());
		rm.setErrorCode(500);
		return new ResponseEntity<ResponseMessage>(rm,HttpStatus.INTERNAL_SERVER_ERROR);	
	}
	
	
	@ExceptionHandler(EmployeeException.class)
	public ResponseEntity<ResponseMessage> handleEmployeeException(HttpServletRequest request, Exception ex){
		ResponseMessage rm = new ResponseMessage();
		rm.setMessage(ex.getMessage());
		rm.setErrorCode(500);
		return new ResponseEntity<ResponseMessage>(rm,HttpStatus.INTERNAL_SERVER_ERROR);	
	}
	
	@ExceptionHandler(IdNotFoundException.class)
	public ResponseEntity<ResponseMessage> handleEmployeeIdException(HttpServletRequest request, Exception ex){
		ResponseMessage rm = new ResponseMessage();
		rm.setMessage(ex.getMessage());
		rm.setErrorCode(404);
		return new ResponseEntity<ResponseMessage>(rm,HttpStatus.NOT_FOUND);	
	}

}
