package com.gl.ems.exception;

public class EmployeeException extends Exception{
	
	
	private String msg;
	
	public EmployeeException(String msg) {
		this.msg= msg;
	}
	@Override
	 public String getMessage() {
	        return this.msg;
	    }
	
	
}
