package com.gl.ems.service;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gl.ems.beans.Employee;

public interface IEmployeeDAO extends JpaRepository<Employee, Integer>{

}
