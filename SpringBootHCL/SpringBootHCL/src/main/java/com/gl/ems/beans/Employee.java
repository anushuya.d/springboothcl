package com.gl.ems.beans;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;


import lombok.*;
@Data
@NoArgsConstructor
@Entity(name = "emp_table")
public class Employee {

	@Id
	@Column(name = "id")
	@GeneratedValue
	private int id;
	private String name;
	//@OneToOne(cascade = CascadeType.ALL)
	//@JoinColumn(name="adr_id")
	//private Address address;
	@ToString.Exclude
	@OneToMany(mappedBy="employee",fetch=FetchType.LAZY)
	private List<Address>address;
	
	@ManyToMany(fetch=FetchType.LAZY,cascade= {CascadeType.PERSIST,CascadeType.MERGE})
	
@JoinTable(name="employee_roles",joinColumns= {
		@JoinColumn(name="emp_id")},
inverseJoinColumns= {@JoinColumn(name="roles_id")})
private List<Roles>roles=new ArrayList<>();

}
