package com.gl.ems.dao;

import java.sql.*;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.gl.ems.beans.Address;
import com.gl.ems.beans.Employee;
import com.gl.ems.exception.EmployeeException;
import com.gl.ems.exception.IdNotFoundException;

@Repository
public class EmployeeDAO {

	@Autowired
	private JdbcTemplate template;

	public String addEmployee(Employee emp) throws EmployeeException{
		List<Employee> list = searchEmployee(emp.getId());
		if (list.size() == 0) {
			String sql = "insert into employee values(?,?)";
			template.update(sql, emp.getId(), emp.getName());
			return "success";
		} else {
			throw new EmployeeException("please enter another id as id already exists");
			
		}

	}

	public List<Employee> displayEmployees() {
		String sql = "select id,name from employee";
		return template.query(sql, new EmployeeRoqMapper());
	}

	public List<Employee> searchEmployee(int id) {
		String sql = "select id,name from employee where id=? ";
		return template.query(sql, new EmployeeRoqMapper(), id);
	}
	
	public String deleteEmployee(Integer id) throws IdNotFoundException{
		String sql = "delete from employee where id=? ";
		int i= template.update(sql, id);
		if(i==0)
			throw new IdNotFoundException("The employee can not be deleted as id is not present");
		else
			return "success";
	}
	
	

	public List<Employee> getEmployeById(Integer id) throws IdNotFoundException{
		// TODO Auto-generated method stub
		String sql = "select id,name from employee where id=? ";
		List<Employee> l = template.query(sql, new EmployeeRoqMapper(), id);
		if(l.size()==0)
			throw new IdNotFoundException("no employee found");
		else
			return template.query(sql, new EmployeeRoqMapper(), id);
	}

	public Employee updateEmployee(Employee emp, int id) throws IdNotFoundException {
		
		String sql= "update employee set name=? where id=?";
		int i =template.update(sql,emp.getName(),id);
		if(i==0)
			throw new IdNotFoundException("no employee found with the given id as "+id);
		else
		{
			return getEmployeById(id).get(0);
		}
			
	}	


}
