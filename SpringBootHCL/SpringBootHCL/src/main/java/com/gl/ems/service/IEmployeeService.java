package com.gl.ems.service;

import java.util.List;

import com.gl.ems.beans.Address;
import com.gl.ems.beans.Employee;
import com.gl.ems.exception.EmployeeException;
import com.gl.ems.exception.IdNotFoundException;

public interface IEmployeeService {

	public Employee addEmployee(Employee emp) throws EmployeeException;

	public List<Employee> displayEmployees();

	public Employee getEmployeById(Integer id) throws IdNotFoundException;

	public String deleteEmployee(Integer id) throws IdNotFoundException;

	public Employee updateEmployee(Employee emp, int id) throws IdNotFoundException;
	
	public List<Address> displayUserAddress(int id) throws IdNotFoundException;
}
