package com.gl.ems.beans;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@Entity(name="roles_tables")
public class Roles {
	
	@Id
	@GeneratedValue
	private int rolesid;
	private String name;
	
	@JsonIgnore
	@ManyToMany(fetch=FetchType.LAZY,cascade= {CascadeType.PERSIST,CascadeType.MERGE},mappedBy="roles")
	
	private List<Employee>employee=new ArrayList<>();

}
