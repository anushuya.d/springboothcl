package com.gl.ems;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import springfox.documentation.swagger2.annotations.EnableSwagger2;
@EnableSwagger2
@SpringBootApplication
public class SpringBootHclApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootHclApplication.class, args);
		System.out.println("hello world");
	}

}
