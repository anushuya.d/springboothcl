package com.gl.ems.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gl.ems.beans.Address;
import com.gl.ems.beans.Employee;
import com.gl.ems.beans.User;
import com.gl.ems.dao.EmployeeDAO;
import com.gl.ems.exception.EmployeeException;
import com.gl.ems.exception.IdNotFoundException;

@Service
public class EmployeeServiceImpl implements IEmployeeService {
	@Autowired
	EmployeeDAO dao;
	@Autowired
	IEmployeeDAO dao1;
    @Autowired
	IAddressDAO dao2;

	@Override
	public List<Employee> displayEmployees() {
		// TODO Auto-generated method stub
		System.out.println("inside service");
		return dao1.findAll();
	}
	
	@Override
	public Employee getEmployeById(Integer id) throws IdNotFoundException {
		// TODO Auto-generated method stub
		Employee emp = dao1.findById(id).orElseThrow(()->new IdNotFoundException("Id not found"));
		return emp;
	}

	@Override
	public String deleteEmployee(Integer id) throws IdNotFoundException {
		// TODO Auto-generated method stub
		Employee emp = dao1.findById(id).orElseThrow(()->new IdNotFoundException("Id not found"));
		dao1.delete(emp);
		return "Success";
	}

	@Override
	public Employee updateEmployee(Employee emp, int id) throws IdNotFoundException {
		// TODO Auto-generated method stub
		dao1.findById(id).orElseThrow(()->new IdNotFoundException("Id not found"));
		return dao1.save(emp);
	}
	
	@Override
	public Employee addEmployee(Employee emp) throws EmployeeException {
		// TODO Auto-generated method stub
		List <Address>adr=emp.getAddress();
		adr.stream().forEach(i->{i.setEmployee(emp);
		
		dao2.save(i);});
		return dao1.save(emp);
	}

	@Override
	public List<Address> displayUserAddress(int id) throws IdNotFoundException {
		// TODO Auto-generated method stub
		
		Employee user=dao1.findById(id).orElseThrow(()->new IdNotFoundException("Id not found"));
		List<Address> adr=user.getAddress();
		return adr;
	}
	
}