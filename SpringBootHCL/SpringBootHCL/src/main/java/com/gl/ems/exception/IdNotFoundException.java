package com.gl.ems.exception;

public class IdNotFoundException extends Exception{
	

	private String msg;
	
	public IdNotFoundException(String msg) {
		this.msg= msg;
	}
	@Override
	 public String getMessage() {
	        return this.msg;
	    }

}
