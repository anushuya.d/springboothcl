package com.gl.ems.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gl.ems.beans.Address;
import com.gl.ems.beans.Employee;
import com.gl.ems.beans.ResponseMessage;
import com.gl.ems.exception.EmployeeException;
import com.gl.ems.exception.IdNotFoundException;
import com.gl.ems.service.IEmployeeService;

@RestController
public class HelloController {

	@Autowired
	IEmployeeService service;

	@RequestMapping(method = RequestMethod.GET, value = "/getEmployees")
	public ResponseEntity<List<Employee>> getEmployees() {

		return new ResponseEntity<List<Employee>>(service.displayEmployees(), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/getEmployee/{id}")
	public ResponseEntity<Employee> getEmployeById(@PathVariable("id") Integer id) throws IdNotFoundException {

		return new ResponseEntity<Employee>(service.getEmployeById(id), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/addEmployee")
	public ResponseEntity<Employee> addEmploye(@RequestBody Employee emp) throws EmployeeException {
		return new ResponseEntity<Employee>(service.addEmployee(emp), HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/deleteEmployee/{id}")
	public ResponseEntity<ResponseMessage> deleteEmploye(@PathVariable("id") int id) throws IdNotFoundException {
		ResponseMessage rm = new ResponseMessage();
		rm.setMessage(service.deleteEmployee(id));
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/updateEmployee/{id}")
	public ResponseEntity<Employee> updateEmploye(@RequestBody Employee emp, @PathVariable("id") int id)
			throws IdNotFoundException {
		return new ResponseEntity<Employee>(service.updateEmployee(emp, id), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/displayUserAddress/{id}")
	
	public List<Address> displayUserAddress(@PathVariable int id )throws IdNotFoundException{
		return service.displayUserAddress(id);
		
	}
	
	
	
	

}