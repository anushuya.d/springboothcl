package com.gl.ems.service;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gl.ems.beans.Address;


public interface IAddressDAO extends JpaRepository<Address, Integer> {

}
